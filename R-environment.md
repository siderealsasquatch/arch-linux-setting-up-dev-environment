# `R` environment setup

Normally I would treat using R the same way I would treat using MATLAB: a console
environment in which to perform some data manipulation and maybe create a few models. Now
that I'm doing some data science work at the office I've been using RStudio a lot and it's
really grown on me. In order to sort of "synchronize" my workflow at home and at the
office, I've decided to work in RStudio when doing stuff with R. To that end, I've decided
to install RStudio on all of my Linux installations, which is a first for me. The rest of
this document details all of the steps required to set up a proper R environment for data
science.

## Some preliminary stuff

Before we even install R, let's make a few minor tweaks that will help when installing R
packages. We'll start by doing two things: enabling the use of all available cores when
compiling and allowing the compiler to perform a caching technique akin to memoization.
Both of these enhancements will decrease compilation times (assuming that the source for
the packages is C/C++). First, check to see if the `ccache` package has been installed on
your system with:

```
pacman -Qi ccache
```

if you get an error, go ahead and install it. The next thing you need to do is add
`ccache` to the PATH. Add the following to your `.bashrc` file:

```
export PATH="/usr/lib/ccache/bin:$PATH"
```

Once this is done, check for an `.R` directory. If it exists, see if there is a `Makevars`
file in it. If any one of those files don't exist, create it. Very quicky check the number
of threads your CPU has with the `nproc` command and then add the following to the
`Makevars` file:

```
MAKEFLAGS=-j<output_of_nproc>
VER=
CCACHE=ccache
CC=$(CCACHE) gcc$(VER)
CXX=$(CCACHE) g++$(VER)
C11=$(CCACHE) g++$(VER)
C14=$(CCACHE) g++$(VER)
FC=$(CCACHE) gfortran$(VER)
F77=$(CCACHE) gfortran$(VER)
```

We want to make sure that the packages we install are installed to our home directory. In
order to acheive this, we'll create a directory `.Rlibs` and a file `.Renviron`. Add the
following to the `.Renviron` file:

```
R_LIBS_USER=~/.Rlibs
```

Now all of the packages installed using the `install.packages` function will be installed
to the `.Rlibs` directory.

## Installing `R` and `RStudio`

Now we can finally install R with:

```
pacman -S R
```

and RStudio with:

```
yay -S rstudio-desktop-bin
```
