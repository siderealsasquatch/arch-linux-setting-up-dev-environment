# Nix - Installation and tutorial

This document outlines the steps necessary for installing the Nix package manager on Arch
Linux as well as provides a basic tutorial on its usage.

## Installation and setup

> The instructions in this section are taken from [here][1]. Review this page if you run
> into any issues.

Start by installing the `nix` and `archlinux-nix` packages from the AUR:

```
yay -S {archlinux-,}nix
```

We'll then need to bootstrap the installation with the following commands:

```
sudo archlinux-nix setup-build-group
sudo archlinux-nix boostrap
```

and then configure a default Nix channel:

```
nix-channel --add https://nixos.org/channels/nixpkgs-unstable
nix-channel --update
nix-env -u
```

The last thing we'll need to do is reboot but if for whatever reason we don't want to or
can't then we'll need to source some Nix related scripts in each shell instance:

```
source /etc/profile.d/nix{,-daemon}.sh
```

We can test the installation with the Nix version of the classic "Hello, World!" program:

```
nix-env -iA nixpkgs.hello
```

Check that `hello` exists in the PATH and that the path to the `hello` package leads to
the nix store.

## Additional tools

In order to make our Nix experience as seamless as possible, we'll need to install the
following tools:

- `lorri`: `nix-shell` alternative that uses a daemon to check for changes in the
  project's `shell.nix` file and update our nix environment in the background.
- `direnv`: works with `lorri` to automatically drop us into a nix environment on `cd`-ing
  into a directory without us having to call `nix-shell` or `lorri`.
- `niv`: pin nix packages to a specific version.

Installation can be done with:

```
nix-env -i lorri direnv niv
```

### `direnv` shell hook

For `zsh` with `oh-my-zsh` it's enough to add the `direnv` plugin to the array of plugins:

```
plugins=(... direnv)
```

### `lorri` setup

Since `lorri` makes use of a daemon we'll need to add the appropriate files to the user
`systemd` directory before enabling the daemon. This can be done in one shot with:

```
wget -P "${XDG_CONFIG_HOME:-$HOME/.config}/systemd/user" \
    https://raw.githubusercontent.com/target/lorri/master/contrib/lorri.service \
    https://raw.githubusercontent.com/target/lorri/master/contrib/lorri.socket && \
    systemctl --user daemon-reload && \
    systemctl --user enable --now lorri.socket
```

Verify that the daemon is running with:

```
systemctl --user is-enabled lorri.socket
# Should see 'enabled'
systemctl --user is-active lorri.socket
# Should see `active`
```

## Example usage

After creating a project directory, normally we'd run `niv` in order to pin the packages
like so:

```
# Initialize the project directory
niv init

# Update the packages
niv update nixpkgs -b nixpkgs-unstable
```

However, in the case of creating an ad-hoc environment for data analysis (rather than some
sort of product) we can probably skip this step and go straight to the lorri
initialization:

```
lorri init
```

This will produce a `.envrc` file and a `shell.nix` file. Edit the `shell.nix` file so
that it looks like the following:

```
{ pkgs ? import <nixpkgs> {} }:

with pkgs;

mkShell {
  name = "lorri_test_no_niv";
  buildInputs = [
    python38
    python38Packages.numpy
    python38Packages.pandas
    python38Packages.scikitlearn
    python38Packages.matplotlib
    python38Packages.seaborn
    python38Packages.ipython
  ];
}
```

Now run:

```
lorri shell
direnv allow
```

`cd` out of and then back into the project directory and you should see your prompt change
to indicate that the nix environment is active. Test the environment by dropping into an
IPython instance and importing some of the Python packages specified in the `shell.nix`
file.


[1]:https://wiki.archlinux.org/index.php/Nix
