# Setting up a Python dev environment

> NOTE: This document is now obsolete since I have switched to asdf for Python version
> management. See the general-dev-environment.md document for details on how to set up a
> Python environment. I'll leave this document here just in case I need/want to move back
> to pyenv.

In the past I used to use the version of python installed on my system in conjunction with
virtualenv-wrapper and for the most part this setup covered my needs. However, I've
decided on a dev environment that offered more flexibility than the one I had before. This
new dev environment makes use of `pyenv` and its plugins `pyenv-virtualenv` and
`pyenv-virtualenvwrapper` to allow me to install multiple versions of Python on my system
and to separate libraries from the default version of python installed on my system. In
addition, I'm able to install commandline programs installable from pip and keep them
separate from all of my virtualenvs using `pipsi`.

## Installing and setting up `pyenv`

The installation is straight-forward and is really a simple matter of following the
instructions given on the `pyenv` github page. Even though it's recommended to use the
`pyenv-installer` script I decided to go with the git clone approach as, at the time, it
seemed that the `pyenv-installer` was no longer being maintained. Start by cloning the
repository with

```
# This is the https method. You should have made ssh keys for your various github and
#gitlab accounts by now.
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
```

Then, add the following lines to your `.bashrc` file

```
# Add these lines anywhere in the body of your .bashrc file except for the end
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

# Add these lines to the very end of your .bashrc file
if command -v pyenv &> /dev/null; then
  eval "$(pyenv init -)"
fi
```

Refresh the current shell session with the following command for the changes to take
effect

```
exec $SHELL
```

Now try running

```
pyenv -h
```

You should see the standard command line program usage information. If this is the case,
you can now use `pyenv` as described in the `pyenv-quick-guide.md` document.

## Installing and setting up `pyenv-virtualenv` and `pyenv-virtualenvwrapper`

As with `pyenv`, the procedure for installing both of these plugins is just a matter of
following the instructions given on the respective github pages.

### `pyenv-virtualenv`

Clone the repo into the `plugins` directory of your `pyenv` installation. Since you cloned
the `pyenv` repository to the default location, this is a simple matter of running the
following command

```
git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
```

Afterwards, enter

```
exec $SHELL
```

to have the changes take effect.

### `pyenv-virtualenvwrapper`

The procedure is pretty much the same as the one for installing `pyenv-virtualenv` with
the only difference being the repo that is cloned into the `pyenv` installation's
`plugins` directory.

```
git clone https://github.com/pyenv/pyenv-virtualenvwrapper.git $(pyenv root)/plugins/pyenv-virtualenvwrapper
```

Again, enter the following for the changes to take effect

```
exec $SHELL
```

## Installing `pyenv-update`

For whatever reason, the `pyenv` repo doesn't come with the `pyenv-update` plugin by
default. You'll need to install it yourself using the following command:

```
git clone https://github.com/pyenv/pyenv-update.git $(pyenv root)/plugins/pyenv-update
```

With this plugin, you'll have access to the `update` command. Double check by running

```
pyenv commands
```

You should see the `update` command in the list of commands as well as the `virtualenv`
and `virtualenvwrapper` commands.

## Installing `pipx`

Just to be safe, make sure you install `pipx` using the system python. From my
understanding, installing `pipx` through the `pip` binary associated with any of your
python versions installed via pyenv will place `pipx` in a separate environment so that
it can be called from anywhere regardless of which version of Python you're currently
using. However, I haven't tried it using another version of Python. Simply enter the
following commands:

```
pyenv shell system
pip install --user pipx
pipx ensurepath
```

Once this is done, you'll want to add the path `/home/fahmi/.local/bin` to the system PATH
manually and remove the line adding the `.local/bin` directory to the path that `pipx`
generated. Open a new terminal and call `pipx -h` to check if `pipx` was successfully
set up.

## Setting up the actual environment

The first thing you'll want to do is check to see what the latest versions of Python3 and
Python2 are using the following command

```
pyenv versions
```

You can then proceed to install the versions you need. In my case, I'm using the latest
version of Python 3 which, at the time of writing, is 3.7.2

```
pyenv install 3.7.2
```

However, if you're using the YouCompleteMe plugin in vim/neovim you'll need to make sure
that you've set the correct options when installing a given version by passing the
`--enable-shared` option as per the following set of commands

```
export PYTHON_CONFIGURE_OPTS="--enable-shared"
pyenv install 3.7.2
```

The next thing you'll need to do is create a virtual environment specifically for the
Python libraries that you need for your Arch Linux setup. In my case, I needed the
libraries `psutil`, `netifaces`, and `pynvim`. I installed all of these libraries in a
virtualenv called `arch-req-libs` using the following commands:

```
pyenv virtualenv 3.7.2 arch-req-libs
pyenv activate arch-req-libs
pip install psutil netifaces pynvim
pyenv deactivate
```

You'll then need to set both the 3.7.2 version of Python and the `arch-req-libs`
virtualenv as the global Python version using the following command:

```
pyenv global arch-req-libs 3.7.2
```

Note the order. If you put 3.7.2 before `arch-req-libs` then you'll run into a situation
where vim/neovim won't be able to detect and load Python. Navigate to the YouCompleteMe
plugin directory and compile the `ycm` module with

```
./install.py --clang-completer
```

The YouCompleteMe plugin should now be functioning correctly. The final step is to install
any Python commandline programs via `pipx`. In my case, I'll install `pywal` since my
Arch Linux set up uses it to set the colorscheme for most of the programs that I use.

```
pipx install pywal
```

You should now have a properly configured Python dev environment that works with your Arch
Linux set up.
