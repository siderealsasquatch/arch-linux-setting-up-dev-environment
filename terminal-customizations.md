# Terminal customizations

## `zsh`

All of this assumes that you've installed `oh-my-zsh`. Remember to replace the github url
with the ssh alias for your `siderealsasquatch` account.

### Autosuggestions

#### Installation instructions

In order to enable auto suggestions similar to `fish` you'll need to install the
`zsh-autosuggestions` using the following command:

```
git clone https://github.com/zsh-users/zsh-autosuggestions \
  ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
```

and then add the `zsh-autosuggestions` plugin to the array of plugins in your `.zshrc`
file:

```
plugins=(... zsh-autosuggestions)
```

#### Brief usage instructions

`zsh-autosuggestions` will provide you with command suggestions as you type. You can
choose to accept the suggestion with the `ctrl+e` keybinding. You can also switch to
navigation mode an use the vim forward and backward motions (w, W, b, B) in order to
accept partial suggestions.

### Setting up syntax highlighting

In order to enable syntax highlighting we'll need to install the
`zsh-syntax-highlighting` plugin using the following command:

```
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git \
  ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```

and then add the `zsh-syntax-highlighting` plugin to the array of plugins in your
`.zshrc` file:

```
plugins=(... zsh-syntax-highlighting)
```

### Enabling `fzf` powered tab completions

We can make it so that tab completions allow for fuzzy matching via `fzf` with the
`fzf-tab` plugin. Simply clone the repo with the following command:

```
git clone https://github.com/Aloxaf/fzf-tab ~ZSH_CUSTOM/plugins/fzf-tab
```

and then add the `fzf-tab` plugin to the array of plugins in your `.zshrc` file:

```
plugins=(... fzf-tab)
```

### Providing a better user experience with Nix

#### Enable Nix completions

Install the `nix-zsh-completions` plugin using the standard procedure for custom plugins:

```
git clone https://github.com/spwhitt/nix-zsh-completions.git \
  ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/nix-zsh-completions
```

and then add it to the array of plugins:

```
plugins=(... nix-zsh-completions)
```

#### Enable the use of the current shell

Usually when `nix-shell` is invoked the user is dropped into a `bash` shell environment
which is usually radically different than the user's current environment. In order to
preserve the user's shell environment when using `nix-shell` we'll need to install
`any-nix-shell` using Nix:

```
# Make sure that you're using the unstable channel
nix-env -i any-nix-shell
```

Then add the following to your `.zshrc`:

```
any-nix-shell zsh | source /dev/stdin
```
