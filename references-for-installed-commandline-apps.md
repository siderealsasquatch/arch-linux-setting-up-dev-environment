# References for installed commandline apps

## General

- [Shell customizations that make my life easier][1]
- [CLI: Improved][2]
- [A Few of my Favorite Things][3]
- [How I overengineered my dotfiles][4]
- [Second-generation interactive shell tools][5]

## git

- [forgit: Interactive Git Commands With Previews Powered By fzf Fuzzy Finder][6]

## vim and neovim

- [Vim Search, Find and Replace: a Detailed Guide][7]
- [Adding syntax highlighting to FZF.vim preview window][8]

## `fzf`

- [Improve your shell with fzf and friends][9]
- [fzf and rg][10]
- [Command Line Fuzzy Find with fzf][11]
- [How to search faster in Vim with FZF.vim][12]
- [Full guide for the use of fuzzy finder][13]

[1]: https://www.cbui.dev/shell-customizations-that-make-my-life-easier/
[2]: https://remysharp.com/2018/08/23/cli-improved
[3]: https://blog.testdouble.com/posts/2020-04-07-favorite-things/
[4]: https://bananamafia.dev/post/dotfiles/
[5]: http://erick.matsen.org/2020/01/04/2nd-gen-interactive-shell.html
[6]: https://www.linuxuprising.com/2019/11/forgit-interactive-git-commands-with.html
[7]: https://thevaluable.dev/vim-search-find-replace/
[8]: https://www.erickpatrick.net/blog/adding-syntax-highlighting-to-fzf.vim-preview-window
[9]: https://hschne.at/2020/04/25/creating-a-fuzzy-shell-with-fzf-and-friends.html
[10]: https://stnly.com/fzf-and-rg/
[11]: https://spin.atomicobject.com/2020/02/13/command-line-fuzzy-find-with-fzf/
[12]: https://dev.to/iggredible/how-to-search-faster-in-vim-with-fzf-vim-36ko
[13]: https://developpaper.com/full-guide-for-the-use-of-fuzzy-finder-fzfvim/
