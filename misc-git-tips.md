# Miscellaneous git tips

Here I detail some git tips that I've found useful. The main purpose of this document is
a sort of quick reference for all of the things related to git that I don't want to
forget or spend time looking up again.

## Pushing to two remote repos simultaneously

There will occasionally be times where you wish to keep two or more remote repositories in
sync with your local repository. This is when I find that it's particularly useful to set
up a git remote with multiple push urls. Let's demonstrate this with a recent use case of
mine. I've taken to using `zsh` with `oh-my-zsh` and wanted to maintain a repository of
cheat sheets to commonly used features provided by `oh-my-zsh` and its plugins. Since I
have accounts on both GitLab and GitHub with the same username I thought it would be a
good idea to push the repo to both of these accounts in the event that I can't access one
or the other service for some reason.

In order to set this up, the first thing I did was create a local repository containing
the cheat sheets. Afterwards, I created repositories on both GitLab and GitHub. The next
thing I did was assign the url to these remote repositories to a name:

```
# Assign urls to remote named 'origin'
git remote add origin gitlab-squatch:siderealsasquatch/oh-my-zsh_cheatsheets.git
git remote set-url --all --push github-squatch:siderealsasquatch/oh-my-zsh_cheatsheets.git

# Assign urls to separate names
git remote add github-squatch github-squatch:siderealsasquatch/oh-my-zsh_cheatsheets.git
git remote add gitlab gitlab-squatch:siderealsasquatch/oh-my-zsh_cheatsheets.git
```

Notice how in addition to assigning both urls to the remote `origin` I also assigned them
to separate remotes named `github-squatch` and `gitlab`. This is so that I can create
branches specific to a particular remote in the event that I need to work with someone
else. With all of this set up, I can push to both remotes simultaneously with a single
command:

```
# Initial push after having just created the remote repos
git push -u origin master

# Subsequent pushes
git push origin master
```

It should be pretty clear that this can be extended to include more than two repos.
