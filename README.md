# Arch Linux dev environment setup

The documents contained in this repo detail how to set up my dev environment in a fresh
Arch Linux installation. So far, the only languages that I'm planning to work extensively
with are R and Python but I can see myself learning new languages in the future. I will
continually add to and revise the documents in this repo so consider everything here an
work in progress.
