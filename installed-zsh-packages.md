# List of installed zsh plugins and associated changes

## `fzf-tab`

Simply installed this package.

## `zsh-autosuggestions`

Removed the `ctrl+space` keybinding.

## `zsh-syntax-highlighting`

Simply installed this package.
