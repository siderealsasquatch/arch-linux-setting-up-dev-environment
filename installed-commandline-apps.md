# Additional commandline apps

Below is a list of all the additional commandline apps that I've installed. Most of them
are meant to be a better replacement to existing apps (like `find` or `grep`).

- `ripgrep` replaces `grep`
- `fd` replaces `find`
- `fzf` for fuzzy searching
- `bat` to replace `cat`
- `jq`
